CREATE TABLE player ( pname VARCHAR(255) PRIMARY KEY );

CREATE TABLE game_score (
                            pname VARCHAR(255) NOT NULL,
                            score INTEGER NOT NULL
);

CREATE VIEW player_total_score_v AS
SELECT
    pname,
    sum(score) AS total_score
FROM game_score
GROUP BY pname;